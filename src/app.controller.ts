import { Controller, Get, Inject } from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern, ClientProxy } from '@nestjs/microservices';
const PLAN_CATALOGUE_SERVICE = 'planCatalogue';
const SERVICE = 'investPerformance';
@Controller()
export class AppController {
  constructor(@Inject('SERVICES') private readonly client: ClientProxy) {}

  @EventPattern({ service: PLAN_CATALOGUE_SERVICE, cmd: 'planUpdated' })
  updatePlan(data) {
    this.client
      .send({ service: SERVICE, cmd: 'updatePlan' }, data.request)
      .toPromise();
  }

  @EventPattern({ service: PLAN_CATALOGUE_SERVICE, cmd: 'planCreated' })
  async createPlan(data) {
    this.client
      .send(
        { service: SERVICE, cmd: 'createPlan' },
        { ...data.request, _id: data.response._id },
      )
      .toPromise();
  }

  @EventPattern({ service: PLAN_CATALOGUE_SERVICE, cmd: 'planDeleted' })
  deletePlan(data) {
    this.client
      .send({ service: SERVICE, cmd: 'deletePlan' }, data.request)
      .toPromise();
  }
}
